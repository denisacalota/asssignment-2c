function addTokens(input, tokens){
    // if(!(input instanceof String)){
    if(typeof input != 'string'){
        throw Error("Invalid input");
    }else if (input.length < 6){
        throw Error("Input should have at least 6 characters");
    }else{
        for(var i=0;i<tokens.length;i++){
            if(typeof tokens[i] != "object" || typeof tokens[i].tokenName !='string'){
                throw Error("Invalid array format");   
            }
        }
        if(input.indexOf("...")<0){
            return input;   
        }else{
            for(var i=0;i<tokens.length;i++){
                // var value1 = ;
                var value2 = input.replace("...", '${'+tokens[i].tokenName+'}');
            }
            return value2;
        }
    }

}



const app = {
    addTokens: addTokens
}

module.exports = app;